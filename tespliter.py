#! /usr/bin/env python3
import os,sys,time

def getSize():
    (height, width) = os.popen("stty size", "r").read().split()
    height = int(height); width = int(width)
    return {"width":width, "height":height, "xmid":int(width/2.0+.5)}

class termsplit:
    channels = {"left":"","right":[""]}
    termsize = getSize()
    HALF_LINE = " "
    def __init__(self):
        self.channels = {"left":"","right":[""]}
        self.termsize = getSize()
        self.HALF_LINE *= self.termsize["xmid"]
        self.channels['right'] = [self.HALF_LINE]
        for i in range(0, self.termsize['height']-1):
            self.channels["right"].append(self.HALF_LINE)
        self.sweepLeft()
        self.clearRight()
        return;
    def write(self, string, channel="left", args=0):
        if channel == "left":
            self.channels[channel]+=string
            self.alignLeft()
            self.clearRight()
            self.wLeft()
            self.wRight()
        elif channel == "right":
            self.channels[channel][args] = string
            self.wRightLine(args)
        else:
            self.channels[channel](self, string, args)
            return;
    def sweepLeft(self):
        self.clearRight()
        sys.stdout.write("\x1b[999;H")
        for i in range(0, self.termsize["height"]+1):
            sys.stdout.write("\n")
        self.wRight()
        sys.stdout.write("\x1b[H\0")
        return;
    def sweep(self):
        sys.stdout.write("\x1b[999;H")
        for i in range(0, self.termsize["height"]+1):
            sys.stdout.write("\n")
        sys.stdout.write("\x1b[H")
        return;
    def clearRight(self):
        sys.stdout.write("\x1b[s")
        string = self.HALF_LINE
        x0 = self.termsize['xmid']
        for i in range(1, self.termsize['height']+1):
            sys.stdout.write("\x1b[{};{}H{}".format(i,x0,string))
        sys.stdout.write("\x1b[H\n\x1b[u")
        return;
    def wRight(self):
        sys.stdout.write("\x1b[s\x1b[H")
        string = self.channels["right"]
        x0 = self.termsize['xmid']-1
        for i in range(0, len(string)):
            sys.stdout.write("\x1b[{};{}H\x1b[32;1m|\x1b[0m{}".format(i+1,x0,string[i]))
        sys.stdout.write("\x1b[H\n\x1b[u")
        return;
    def wRightLine(self, line=0):
        sys.stdout.write('\x1b[s\x1b[{};{}H{}\x1b[H\n\x1b[u'\
                .format(line+1, self.termsize['xmid'], self.channels['right'][line]))
        return;
    def wLeft(self):
        sys.stdout.write(self.channels['left'])
        self.channels['left'] = ""
        return;
    def align(self, string, sizeOfLine):
        sol = sizeOfLine-2
        strings = string.split('\n')
        tstring = ""
        for s in strings:
            last=0
            for i in range(sol, len(s)+sol, sol):
                tstring+=s[last:min(i,len(s))]+'\n'
                last=i
        return tstring;
    def alignLeft(self):
        self.channels['left'] = self.align(self.channels['left'],self.termsize['xmid'])
        return;
    def addChannel(self, channel, channelPrintFunc):
        self.channels[channel]=channelPrintFunc
        return;
    
l = termsplit()
def writeErr(tesp, string, args):
    tesp.write('\x1b[31;1m'+string+'\x1b[0m')
    return;
l.addChannel('err', writeErr)
l.write('hello world this line will end up being split into different lines because this is a reeeeabcdefghijklmnopallly long line of text!!?!!','left')
l.write('this is an error message added to the class termsplit', 'err')
l.write('this is the right channel','right',4)
l.write('this should be higher','right')
for i in range(0, 40):
    l.write('this is {} of 40 left print statements'.format(i))

time.sleep(2)

