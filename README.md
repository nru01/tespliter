# terminal_spliter-py

python class to allow splitting the output of the terminal window into two streams/channels.
	left channel:
		works like the normal output stream but takes up half the terminal.
	right channel:
		static display of lines that remain on screen until cleared or modified.
example:
```
hello world this line will end up being split into different lines because this is a reeeeabc|this should be higher                                                                          
defghijklmnopallly long line of text!!?!!                                                    |                                                                                               
this is an error message added to the class termsplit                                        |                                                                                               
this is 0 of 40 left print statements                                                        |                                                                                               
this is 1 of 40 left print statements                                                        |this is the right channel                                                                      
this is 2 of 40 left print statements                                                        |                                                                                               
this is 3 of 40 left print statements                                                        |                                                                                               
this is 4 of 40 left print statements                                                        |                                                                                               
this is 5 of 40 left print statements                                                        |                                                                                               
this is 6 of 40 left print statements                                                        |                                                                                               
this is 7 of 40 left print statements                                                        |                                                                                               
this is 8 of 40 left print statements                                                        |                                                                                               
this is 9 of 40 left print statements                                                        |                                                                                               
this is 10 of 40 left print statements                                                       |                                                                                               
this is 11 of 40 left print statements                                                       |                                                                                               
this is 12 of 40 left print statements                                                       |                                                                                               
this is 13 of 40 left print statements                                                       |                                                                                               
this is 14 of 40 left print statements                                                       |                                                                                               
this is 15 of 40 left print statements                                                       |                                                                                               
this is 16 of 40 left print statements                                                       |                                                                                               
this is 17 of 40 left print statements                                                       |                                                                                               
this is 18 of 40 left print statements                                                       |                                                                                               
this is 19 of 40 left print statements                                                       |                                                                                               
this is 20 of 40 left print statements                                                       |                                                                                               
this is 21 of 40 left print statements                                                       |                                                                                               
this is 22 of 40 left print statements                                                       |                                                                                               
this is 23 of 40 left print statements                                                       |                                                                                               
this is 24 of 40 left print statements                                                       |                                                                                               
this is 25 of 40 left print statements                                                       |                                                                                               
this is 26 of 40 left print statements                                                       |                                                                                               
this is 27 of 40 left print statements                                                       |                                                                                               
this is 28 of 40 left print statements                                                       |                                                                                               
this is 29 of 40 left print statements                                                       |                                                                                               
this is 30 of 40 left print statements                                                       |                                                                                               
this is 31 of 40 left print statements                                                       |                                                                                               
this is 32 of 40 left print statements                                                       |                                                                                               
this is 33 of 40 left print statements                                                       |                                                                                               
this is 34 of 40 left print statements                                                       |                                                                                               
this is 35 of 40 left print statements                                                       |                                                                                               
this is 36 of 40 left print statements                                                       |                                                                                               
this is 37 of 40 left print statements                                                       |                                                                                               
this is 38 of 40 left print statements                                                       |                                                                                               
this is 39 of 40 left print statements                                                       |                                                                                               
[_@_ tespliter]$                                                                             |                                                                                               

```
